#!/bin/bash

function clearLog () {
    clean=`rm /var/log/logZenity.log`
    echo "Log limpo"
}

function seeLog () {
    zenity --width=500 --height=500 --info --title="Log" --text=`cat /var/log/logZenity.log`
}

function PID(){
    fname="Inserir PID"
    regexNumber='^[0-9]+$'

    DADOS=$(zenity --width=500 --height=500 --forms --title="Projeto PS" --text="Insira o PID" \
    --separator="," \
    --add-entry="PID: ")
    
    PID=`echo $DADOS | cut -d, -f1`
    
    if ! [[ $PID =~ $regexNumber ]] ; 
        then zenity --error --title="PID" --text="Envie um PID válido" --width=150 --height=200
    else
        
        return $PID
    fi

    echo "`date` user: `hostname` executed command: $fname" >     /var/log/logZenity.log
} 

function command(){
    fname="Comando utilizado para executar o processo"
    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="Comando executado: ps -aux -q $PID" 
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
    }

function seeUser(){
    fname="Nome do Usuário que executou o processo"
    
    middle="echo `ps -aux -q $PID | cut -c 1-5`"
    user=`echo $middle | cut -c 10-`
    
    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="Nome do Usuario: $user" 
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
}

function seePID(){
    fname="PID e nome do processo pai"
    
    middle="echo `ps -F -q $PID | cut -c 1-5`"
    user=`echo $middle | cut -c 10-`

    middle2="echo `ps -F -q $PID | cut -c 15-20`"
    ppid2=`echo $middle2 | cut -c 10-`
    echo $ppid2

    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="Nome do Processo Pai: $user. PPID: $ppid2"
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
}

function ForeGround(){
    fname="Foregroud Background"            
    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="`ps -F`" 
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
}

function searchUser(){
    fname="Procurar processos de um usuario"   
    
    sUser=$(zenity --width=500 --height=500 --forms --title="Buscar Por Usuario" --text="Insira o usuario" \
    --separator="," \
    --add-entry="Nome: ")
    
    sUser2=`echo $sUser | cut -d, -f1`

    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="`ps -au | grep $sUser2`" 
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
}

function searchName(){
    fname="Procurar processos por nome"       

    sName=$(zenity --width=500 --height=500 --forms --title="Buscar Por Nome" --text="Insira o nome" \
    --separator="," \
    --add-entry="Nome: ")
    
    name=`echo $sName | cut -d, -f1`

    zenity --width=500 --height=500 --info --title="NomeProcesso" --text="`ps -au | grep $name`" 
    echo "`date` user: `hostname` executed command: $fname" >>  /var/log/logZenity.log
}
