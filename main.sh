#!/bin/bash

source ./controller.sh 

clearLog

while true; do
choice="$(zenity --width=500 --height=500 --list --column "" --title="Projeto: PS" \
    "1 - Listar Processos existentes" \
    "2 - Inserir PID" \
    "3 - PID e nome do processo pai" \
    "4 - Comando utilizado para executar o processo" \
    "5 - Nome do Usuário que executou o processo" \
    "6 - Procurar processos de um usuario" \
    "7 - Procurar processos por nome" \
    "8 - Visualizar log ")"
    case "${choice}" in
        "2 - Inserir PID" )
            PID
        ;;
        "4 - Comando utilizado para executar o processo" )
            command
        ;;
        "5 - Nome do Usuário que executou o processo" )
            seeUser
        ;;
        "3 - PID e nome do processo pai" )
            seePID
        ;;
        "1 - Listar Processos existentes" )
            ForeGround
        ;;
        "6 - Procurar processos de um usuario" )
            searchUser
        ;;
        "7 - Procurar processos por nome" )
            searchName
        ;;
        "8 - Visualizar log " )
            seeLog
        ;;
        *)
            break
        ;;
    esac
done
        